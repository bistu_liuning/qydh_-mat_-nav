%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% @brief: 系统运行参数设置
% @File Name: UserSetting.m
% @Project Name: QYDH
% @Author: Liu Ning
% @CopyRight: 版权归北京信息科技大学高动态导航技术北京市重点实验室所有，最终解释权归该高动态导航技术北京市重点实验室所有
%            Copyright (c) 2020 Beijing key library of High dynamic navigation technology, ISC License (open source)
% @Description:
% 
% @Content
% Data      Author     Notes
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 主函数
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% funtion u=UserSetting() 
% @brief 进行系统设置和惯导数据导入接口设置   
% @param[out]  u 惯导数据，包含x、y、z陀螺仪和角速度、地磁信息 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [u] = UserSetting(filename)
%% 定义全局变量，全局内的系统仿真参数设置
global simdata;
%% 设置模型参数
simdata.ModelMethod   = 'SVM';
simdata.ModelWorkType = 'Train';
%% 设置标签参数
simdata.LabelFileType = 'txt';

%% 设置数据参数
% txt dat mat xls xlsx
fileNameLength=length(filename);
str=sprintf('%c%c%c',filename(fileNameLength-2),filename(fileNameLength-1),filename(fileNameLength));
simdata.DataFileType    = str ;

%% 进行数据导入
if simdata.DataFileType == 'dat'
    u = importdata(filename);
elseif simdata.DataFileType == 'txt' 
    u = importdata(filename); 
elseif simdata.DataFileType == 'xls'
    u = xlsread(filename);
elseif simdata.DataFileType == 'lsx'
    u = xlsread(filename);
elseif simdata.DataFileType == 'mat'
    u = importdata(filename);
end

end

