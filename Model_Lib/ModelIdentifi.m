%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% @filename ModelIdentifi.m  
% @brief 模型辨识与分类
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [output] = ModelIdentifi(u)

global simdata;

if simdata.ModelWorkType == 'Train'

    % 导入训练数据
    X_train = importdata('X_train.txt');
    y_train = importdata('y_train.txt'); 
    X_test  = importdata('X_test.txt'); 
    y_test  = importdata('y_test.txt');
    
    % 转换成分类标签向量
    y_train_transpose = transpose(y_train);
    y_test_transpose = transpose(y_test);
    
    % 初始化SVM参数
    % SVM分类为6类
    % 设置测试样本点数为2947个
    SVMModel = cell(6,1);
    label = zeros(6,2947);
    
    % 分类数据设置
    trainingClassLabelsMatrix = full(ind2vec(y_train_transpose,6));
    
    % 训练模型转换
    for index=1:6
        SVMModel{index} = fitcsvm(X_train,trainingClassLabelsMatrix(index,:),'KernelFunction','polynomial','PolynomialOrder',2);
    end
    
    % 进行标签预测
    for index=1:6
        label(index,:) = predict(SVMModel{index},X_test);
    end
    
    % 进行指标转换
    predictedLabel=vec2ind(label);
    
    %计算推理精度
    accuracy = sum(y_test_transpose == predictedLabel)/length(y_test_transpose);
    accuracyPercentage = 100*accuracy;
    fprintf('推理精度为： %f%%\n',accuracyPercentage)

    save SVMModelData;

    output = ones(length(u),1);

elseif simdata.ModelWorkType == 'Predict'
    output = ones(length(u.IMU_Data),1);
end
end

