%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% @brief: 运动分类与模型统一
% @File Name: main.m
% @Project Name: QYDH
% @Author: Liu Ning
% @Date          Author          Notes
% 20220420      Liu Ning        1、创建对应工程 
% 20220426      Liu Ning        1、提供南京理工大学
% 20220514      Liu Ning        1、
%                               
% @CopyRight: 版权归北京信息科技大学高动态导航技术北京市重点实验室所有，最终解释权归该高动态导航技术北京市重点实验室所有
%            Copyright (c) 2022 Beijing key library of High dynamic navigation technology, ISC License (open source)
% 技术问题可发送问题至：liuning1898@qq.com，刘宁，13810655202
% @details:
% 程序运行主程序，主要包括：系统设置、数据导入、模型检测、导航估计、结果显示
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 1. 系统初始化，清除系统内存
clear all;
clc;
close all;
%% 2. 增加库文件路径
addpath('Setting_Lib');         % 系统设置 库目录
addpath('Model_Lib');           % 模型检测 库目录
addpath('Nav_Lib');             % 导航方法 库目录
addpath('Coordinate_Lib');      % 模型转换 库目录
addpath('Report_Lib');          % 报告生成 库目录
addpath('DataRe')
global simdata;                 % 用于存放全局变量
%% 3. 指定数据文件
fileInName  ='Sensor_data.mat';
fileOutName ='DataOut';
fileLabelName ='LabelOut.dat';
%% 4. 执行程序设置与数据导入
disp('4. 执行程序设置与数据导入');
% u为数据输入变量
[u]  = UserSetting(fileInName);
%% 5. 运行模型检测
disp('5. 运行模型检测');
% m为模型辨识结果
[m]  = ModelIdentifi(u);
%% 6. 模型归一
disp('6. 模型归一');
% t为模型归一数据输出
[t]  = ModelSolve(u,m);
%% 7. 执行导航方法
disp('7. 执行导航方法');
% x_h为导航解算输出
[simdata.DataOut]= NavMethod(u,m,t);
%% 8. 数据显示与报告生成
disp('8. 数据显示与报告生成');
ViewData;
% GenReport(fileOutName,[u,m,t,simdata.Data]);