%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% @filename GenReport.m  
% @brief 曲线输出
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [output] = GenReport(filename,Data)
%% 创建文件
strt=sprintf('%s.dat',filename);
fid=fopen(strt,'wb');
[row,col]=size(Data);
%% 数据导出
for i=1:1:row
    for j=1:1:col
        fprintf(fid,'%.8f ',Data(i,j));
        if(j==col)
            fprintf(fid,'\n');
        end
    end
end
fclose(fid);
end

