%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% @filename view_data.m  
% @brief 曲线输出
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
global simdata;
Data=simdata.DataOut;
GPSLat=Data.GPS_Data(:,2);
GPSLon=Data.GPS_Data(:,3);
GPSAlt=Data.GPS_Data(:,4);

figure
plot(GPSLon,GPSLat,'r*');
legend('GNSS');

