# QYDH_Matlab程序

## 摘要

### 文件名称

Readme.md

### 项目名称

QYDH_Matalab

### 项目概述

1. 用于QYDH项目相关算法验证；
2. 主要负责载体（人、车、机）运动分类；
3. 进行模型统一，通过规范的输入，生成特定的输出。

### 作者信息

刘宁，liuning1898@qq.com , 13810655202

### 版权信息

1. 版权归北京信息科技大学高动态导航技术北京市重点实验室所有，最终解释权归该高动态导航技术北京市重点实验室所有
2. Copyright (c) 2020 Beijing key library of High dynamic navigation technology, GLL 2.0 License (open source)

## 待调试内容

1. 增加模型辨识对比
2. 按照总体要求调整模型统一参数

## BUG记录

暂未发现

## 修改纪要

#### 【2205141】

修改时间：2022年5月14日

贡献人：刘宁

1. 适配总体给的数据协议格式，完成接口转换
2. 未增加必要算法，等具体通知

#### 【22042601】

修改时间：2022年4月26日

贡献人：刘宁

1. 调整程序接口，增加必要注释

2. 数据说明

   01_52657375-20211119103546_#02_small_circle3.dat：

   2号设备，前后天线正装（与姿态角相反），小圈轨迹，3圈。

   02_52657375-20211119105443_#02_small_circle2_opp.dat：

   2号设备，前后天线反装（与姿态角相同），小圈轨迹，2圈。

   03_52657375-20211119111049_#01_big_circle1_opp.dat

   1号设备，前后天线反装（与姿态角相同），大圈轨迹，1圈。

   04_52657375-20211119112508_#03_river_circle1_opp.dat

   3号设备，前后天线反装（与姿态角相同），河边轨迹（存在丢星），1圈。

   05_52657375-20211119113545_#03_lab2bistu_one1_opp.dat

   3号设备，前后天线反装（与姿态角相同），lab到bistu，单程。

3. 通道说明

| 序号 | 标识      | 说明           | 单位 | 备注                       |
| ---- | --------- | -------------- | ---- | -------------------------- |
| 1    | TIME      | 系统时间       | ms   | 采样频率200Hz              |
| 2    | AX1_RC    | x轴加速度      | m/s2 | 加速度输出                 |
| 3    | AY1_RC    | y轴加速度      | m/s2 |                            |
| 4    | AZ1_RC    | z轴加速度      | m/s2 |                            |
| 5    | GX_RC     | x轴陀螺仪      | °/s  |                            |
| 6    | GY_RC     | y轴陀螺仪      | °/s  |                            |
| 7    | GZ_RC     | z轴陀螺仪      | °/s  |                            |
| 8    | MX_RC     | x轴地磁        |      | 数字值输出，未进行量纲转换 |
| 9    | MY_RC     | y轴地磁        |      |                            |
| 10   | MZ_RC     | z轴地磁        |      |                            |
| 11   | GPS_LONGT | GNSS经度       | °    |                            |
| 12   | GPS_LAT   | GNSS纬度       | °    |                            |
| 13   | GPS_ALT   | GNSS高度       | °    |                            |
| 14   | GPS_VE    | GNSS东向速度   | m/s  |                            |
| 15   | GPS_VN    | GNSS北向速度   | m/s  |                            |
| 16   | GPS_SATN  | GNSS卫星数     |      |                            |
| 17   | GPS_PDOP  | GNSS pdop值    |      |                            |
| 18   | BARO_PRE  | 压强           | MPa  |                            |
| 19   | BARO_ALT  | 压强计算高度   | m    |                            |
| 20   | INSVN     | 组合后北向速度 | m/s  |                            |
| 21   | INSVE     | 组合后东向速度 | m/s  |                            |
| 22   | INSVD     | 组合后地向速度 | m/s  |                            |
| 23   | INSPN     | 组合后北向位置 | m    |                            |
| 24   | INSPE     | 组合后东向位置 | m    |                            |
| 25   | INSPD     | 组合后地向位置 | m    |                            |
| 26   | GPS_DESX  | GNSS 北向位置  | m    | 消除原点坐标               |
| 27   | GPS_DESY  | GNSS 东向位置  | m    |                            |
| 28   | GPS_DESZ  | GNSS 地向位置  | m    |                            |
| 29   | GPS_VX    | 组合后北向位置 | m    |                            |
| 30   | GPS_VY    | 组合后东向位置 | m    |                            |
| 31   | GPS_VZ    | 组合后地向位置 | m    |                            |
| 32   | PHI       | 偏航角         | °    |                            |
| 33   | THETA     | 俯仰角         | °    |                            |
| 34   | GAMMA     | 滚转角         | °    |                            |
| 35   | GPS_HDT   | GNSS航向角     | °    |                            |
| 36   | TEMP      | 温度           | °    |                            |

5. 采集设备 说明，具体见设备说明：双天线组合导航产品指标书_21111801.pdf。

#### 【22042001】

修改时间：2022年4月20日

贡献人：刘宁

1. 按照总体要求搭建基本框架
2. 确定整体思路，因要适应Matlab版本，故舍弃深度学习相关方法，采用SVM方法

## 技术支持热线

1. 邮箱：liuning1898@qq.com
2. 手机：13810655202
3. Gitee网址：https://gitee.com/bistu_liuning

